package com.example.l.routeplanning;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import java.util.HashMap;

public class MainActivity extends Activity {
    Bitmap bmp, bmp2;
    ImageView img;
    HashMap<Integer, Float> x = new HashMap<Integer, Float>();
    HashMap<Integer, Float> y = new HashMap<Integer, Float>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testlayout);

        img = (ImageView) findViewById(R.id.imageView);

        for (int i = 0; i < 5; i++) {
            x.put(i, (float) 220 + i * 70);
            y.put(i, (float) 840 - i * 80);
        }

        Resources res = getResources();
        bmp = BitmapFactory.decodeResource(res, R.drawable.a123);
        bmp2 = BitmapFactory.decodeResource(res, R.drawable.a789);


        img.setImageBitmap(bmp);

        for (int i = 0; i < 5; i++) {
            img.setImageBitmap(bmp = createBitmap(bmp, bmp2, x.get(i), y.get(i)));
        }
    }

    private Bitmap createBitmap(Bitmap src, Bitmap point, float x, float y) {
        Log.e("test", "create a new bitmap X " + x + "Y " + y);
        if (src == null) {
            return null;
        }
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap newb = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas cv = new Canvas(newb);
        cv.drawBitmap(src, 0, 0, null);
        cv.drawBitmap(point, x, y, null);
        cv.save(Canvas.ALL_SAVE_FLAG);
        cv.restore();
        return newb;
    }
}
